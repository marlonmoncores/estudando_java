package br.com.caelum.livraria.bean;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
/*
 * Tem que anotar com rollback=true porque por padrão o container EJB não dá rollback em unchecked exceptions...
 */
public class LivrariaException extends RuntimeException {

}
