package br.com.caelum.livraria.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.caelum.livraria.modelo.Livro;

@Stateless
public class LivroDao {

	@PersistenceContext
	private EntityManager manager;
	
	public void salva(Livro livro) {
		manager.persist(livro);
	}
	
	public List<Livro> todosLivros() {
		return manager.createQuery("select l from Livro l", Livro.class).getResultList();
	}

	@WebResult(name="livros")
	public List<Livro> livrosPeloNome(@WebParam(name="titulo")String nome) {
		return manager.createQuery("select l from Livro l where l.titulo like :pTitulo", Livro.class).setParameter("pTitulo", nome).getResultList();
	}
	
}
