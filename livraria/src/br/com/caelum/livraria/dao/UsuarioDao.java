package br.com.caelum.livraria.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.caelum.livraria.modelo.Usuario;

@Stateless
public class UsuarioDao {

	@PersistenceContext
	private EntityManager manager;

	public Usuario buscaPeloLogin(String login) {
		List<Usuario> foundData =  
				this.manager.createQuery("select u from Usuario u where u.login=:login",Usuario.class)
				.setParameter("login", login)
				.getResultList();
		return foundData.isEmpty()? null : foundData.get(0);
		/*
		TypedQuery<Usuario> tq = this.manager.createQuery("select u from Usuario u where u.login=:login",Usuario.class);
		tq.setParameter("login", login);
		List<Usuario> foundData = tq.getResultList();
		return foundData.isEmpty()? null : foundData.get(0);
		*/
	}
	
}
