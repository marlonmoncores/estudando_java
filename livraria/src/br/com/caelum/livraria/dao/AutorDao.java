package br.com.caelum.livraria.dao;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import br.com.caelum.livraria.interceptador.LogInterceptador;
import br.com.caelum.livraria.modelo.Autor;

@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER) //esse é o valor padrao
//@TransactionManagement(TransactionManagementType.BEAN) //Nao é usual, deixa o controle da transaction no bean
//@Interceptors ({LogInterceptador.class})
public class AutorDao {

	@PersistenceContext
	private EntityManager manager;
	
//	@Inject	
//	UserTransaction tx;

	@PostConstruct
	private void aposCriacao(){
		System.out.println("[INFO] AutorDao foi criado.");
	}
	
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)//esse é o valor padrao
	@TransactionAttribute(TransactionAttributeType.MANDATORY) //obriga a ter uma transacao aberta
//	@TransactionAttribute(TransactionAttributeType.NEVER) //proibe que tenha uma transacao
	public void salva(Autor autor) {
		System.out.println("[INFO] Salvando o Autor " + autor.getNome());
		
		manager.persist(autor);
		
		System.out.println("[INFO] Salvou o Autor " + autor.getNome());
//		throw new RuntimeException ("teste de erro");
	}
	/* metodo controlando a sua propria transação. nao recomendado
	public void salva(Autor autor) {
		System.out.println("[INFO] Salvando o Autor " + autor.getNome());
		try{
			tx.begin();
			manager.persist(autor);
			tx.commit();
		}catch (Exception e){
			e.printStackTrace();
		}
			
			
		
		System.out.println("[INFO] Salvou o Autor " + autor.getNome());
	}*/
	
	public List<Autor> todosAutores() {
		return manager.createQuery("select a from Autor a", Autor.class).getResultList();
	}

	public Autor buscaPelaId(Integer autorId) {
		Autor autor = this.manager.find(Autor.class, autorId);
		return autor;
	}
	
}
