package br.com.caelum.livraria.interceptador;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class LogInterceptador {

	@AroundInvoke
	public Object intercepta(InvocationContext context) throws Exception{
		long millis = System.currentTimeMillis();
		
		//chamada do metodo DAO
		Object o = context.proceed();
		String metodo = context.getMethod().getName();
		String nameClass = context.getTarget().getClass().getSimpleName();
		System.out.println(nameClass + " - "+ metodo+ " - "+"Tempo gasto:" + (System.currentTimeMillis() - millis));
		
		return o;
	}
}
