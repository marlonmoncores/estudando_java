package br.com.caelum.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.caelum.model.Categoria;
import br.com.caelum.model.Loja;
import br.com.caelum.model.Produto;

@Repository
public class ProdutoDao {

	@PersistenceContext
	private EntityManager em;

	public List<Produto> getProdutos() {
		return em.createQuery("from Produto", Produto.class).getResultList();
	}

	public Produto getProduto(Integer id) {
		Produto produto = em.find(Produto.class, id);
		return produto;
	}

	public List<Produto> getProdutos_(String nome, Integer categoriaId, Integer lojaId) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Produto> query = criteriaBuilder.createQuery(Produto.class);
		query.distinct(true);//evita elementos duplicados, mesmo com a join
		Root<Produto> root = query.from(Produto.class);

		Path<String> nomePath = root.<String> get("nome");
		Path<Integer> lojaPath = root.<Loja> get("loja").<Integer> get("id");

		List<Predicate> predicates = new ArrayList<>();

		if (!nome.isEmpty()) {
			Predicate nomeIgual = criteriaBuilder.like(nomePath, "%"+nome+"%");
			predicates.add(nomeIgual);
		}
		if (categoriaId != null) {
			Path<Integer> categoriaPath = root.join("categorias").<Integer> get("id");//só faz o join se for necessário
			Predicate categoriaIgual = criteriaBuilder.equal(categoriaPath, categoriaId);
			predicates.add(categoriaIgual);
		}
		if (lojaId != null) {
			Predicate lojaIgual = criteriaBuilder.equal(lojaPath, lojaId);
			predicates.add(lojaIgual);
		}

		query.where((Predicate[]) predicates.toArray(new Predicate[0]));

		TypedQuery<Produto> typedQuery = em.createQuery(query);
		return typedQuery.getResultList();

	}
	
	
	public List<Produto> getProdutos(String nome, Integer categoriaId, Integer lojaId) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Produto> query = criteriaBuilder.createQuery(Produto.class);
		query.distinct(true);//evita elementos duplicados, mesmo com a join
		Root<Produto> root = query.from(Produto.class);

//		List<Predicate> predicates = new ArrayList<>();
		Predicate predicate = criteriaBuilder.conjunction();

		if (!nome.isEmpty()) {
			Path<String> nomePath = root.<String> get("nome");
			Predicate nomeIgual = criteriaBuilder.like(nomePath, "%"+nome+"%");
			predicate = criteriaBuilder.and(predicate, nomeIgual);
		}
		if (categoriaId != null) {
			Path<Integer> categoriaPath = root.join("categorias").<Integer> get("id");//só faz o join se for necessário
			Predicate categoriaIgual = criteriaBuilder.equal(categoriaPath, categoriaId);
			predicate = criteriaBuilder.and(predicate, categoriaIgual);
		}
		if (lojaId != null) {
			Path<Integer> lojaPath = root.<Loja> get("loja").<Integer> get("id");
			Predicate lojaIgual = criteriaBuilder.equal(lojaPath, lojaId);
			predicate = criteriaBuilder.and(predicate, lojaIgual);
		}

		query.where(predicate);

		TypedQuery<Produto> typedQuery = em.createQuery(query);
		return typedQuery.getResultList();
	}

	public void insere(Produto produto) {
		if (produto.getId() == null)
			em.persist(produto);
		else
			em.merge(produto);
	}

}
